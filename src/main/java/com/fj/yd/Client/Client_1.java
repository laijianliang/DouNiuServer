package com.fj.yd.Client;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Client_1 {

    public static void main(String[] args) throws IOException {

        EventLoopGroup group = new NioEventLoopGroup();
        Bootstrap b = new Bootstrap();
        b.group(group)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline p = ch.pipeline();
                        ByteBuf byteBuf = Unpooled.copiedBuffer("_end".getBytes());
                        p.addLast(new DelimiterBasedFrameDecoder(1024,byteBuf));
                        p.addLast(new StringDecoder(CharsetUtil.UTF_8));
//                        p.addLast(new StringEncoder(CharsetUtil.UTF_8));
//                        p.addLast("stringEncoder", new StringEncoder(CharsetUtil.UTF_8));
//                        p.addFirst(new LoginClientHandler());
//                        p.addLast(new HelloClientHandler());
                        p.addLast(new ServerClientHandler());

                    }
                });
        try {
            Channel channel = b.connect("localhost", 1111).sync().channel();

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                channel.writeAndFlush(Unpooled.copiedBuffer((reader.readLine() + "_end").getBytes()));
            }


        } catch (InterruptedException e) {
            e.printStackTrace();
        }  finally {
            // Shut down the event loop to terminate all threads.
            group.shutdownGracefully();
        }
    }
}
