package com.fj.yd.Client;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.util.CharsetUtil;

import java.io.IOException;

public class StartClient {
    
    public void start(String host,int port,String message) throws IOException {

        EventLoopGroup group = new NioEventLoopGroup();
        Bootstrap b = new Bootstrap();

        Bootstrap handler = b.group(group)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline p = ch.pipeline();
                        ByteBuf byteBuf = Unpooled.copiedBuffer("_end".getBytes());
                        p.addLast(new DelimiterBasedFrameDecoder(1024, byteBuf));
                        p.addLast(new StringDecoder(CharsetUtil.UTF_8));
//                        p.addLast(new StringEncoder(CharsetUtil.UTF_8));
//                        p.addLast("stringEncoder", new StringEncoder(CharsetUtil.UTF_8));
//                        p.addFirst(new LoginClientHandler());
//                        p.addLast(new HelloClientHandler());
                        p.addLast(new ServerClientHandler());

                    }
                });
        try {
            Channel channel = b.connect(host, port).sync().channel();

            channel.writeAndFlush(Unpooled.copiedBuffer((message+ "_end").getBytes()));
            channel.closeFuture().sync();


        } catch (InterruptedException e) {
            e.printStackTrace();
        }  finally {
            // Shut down the event loop to terminate all threads.
            group.shutdownGracefully();
        }
    }
   
}

