package com.fj.yd.commons;

import com.fj.yd.pojo.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcTemplate_User {



    public static User QueryByUsername(String sql, User user)
    {
        JdbcUtil tj = new JdbcUtil();
//
//        String sql2 = "delete from tb_user  where username =?";
//        Object[] obj2 = {"baidu"};
//        int i = tj.updateAndDeleteData(sql2, obj2);
//        System.out.println(i);
//        String sql = "select * from tb_user";
        Object[] obj ={user.getUsername()};

        User user1 = new User();
        ResultSet rs = tj.queryData(sql, obj);
        try {

            while (rs.next()) {

                user1.setUsername( rs.getString(1));
                user1.setPassword(rs.getString(2));

            }
            return user1;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            tj.closeConnction();
        }
        return user1;
    }

    public static int InsertUser(String sql, User user)
    {
        JdbcUtil tj = new JdbcUtil();
        Object[] obj ={user.getUsername(),user.getPassword()};
        int rs = tj.updateAndDeleteData(sql, obj);
        return rs;
    }
}
