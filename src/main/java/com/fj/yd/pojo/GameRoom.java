package com.fj.yd.pojo;

import io.netty.channel.Channel;
import java.util.ArrayList;
import java.util.List;

public class GameRoom {

    private int id;


    private List<Channel> channels = new ArrayList<Channel>();

    public GameRoom() {
        this.id = (int) Math.round((Math.random()+1) * 1000);

    }

    public void addChannel(Channel channel)
    {
        channels.add(channel);
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Channel> getChannels() {
        return channels;
    }

    public void setChannels(List<Channel> channels) {
        this.channels = channels;
    }
}
