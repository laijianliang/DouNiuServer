package com.fj.yd.Server;

import com.fj.yd.commons.ConvertByteBufToString;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;

public class GameServer {

    public static void main(String[] args) {

        NioEventLoopGroup bosseventLoopGroup = new NioEventLoopGroup();
        NioEventLoopGroup childeventLoopGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bosseventLoopGroup,childeventLoopGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline p = ch.pipeline();
                            ByteBuf byteBuf = Unpooled.copiedBuffer("_end".getBytes());
                            p.addLast(new DelimiterBasedFrameDecoder(1024,byteBuf));
                            p.addLast( new StringDecoder(CharsetUtil.UTF_8));
//                            p.addLast(new StringEncoder(CharsetUtil.UTF_8));

//                            p.addLast("stringEncoder", new StringEncoder(CharsetUtil.UTF_8));
//                            p.addFirst(new LoginServerHandler());
                            p.addLast(new ServerHandler());

                        }
                    });
            bootstrap.bind(1111).sync().channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // Shut down the event loop to terminate all threads.
            bosseventLoopGroup.shutdownGracefully();
            childeventLoopGroup.shutdownGracefully();
        }


    }

}
