package com.fj.yd.Server.Accept;

import com.fj.yd.commons.JdbcTemplate_User;
import com.fj.yd.commons.ResultMessage;
import com.fj.yd.pojo.User;
import io.netty.buffer.ByteBuf;

/**
 * 处理登录请求
 */
public class AcceptLogin {


    public static ByteBuf handleLogin(String content)
    {
        ResultMessage resultMessage = new ResultMessage();
        User user = new User();
        //获取content中的username的值 :"login-username:admin&password:123456-_end"
        user.setUsername(content.split("&")[0].split(":")[1]);
        //获取content中的password的值
        String password =content.split("&")[1].split(":")[1];
        User result = JdbcTemplate_User.QueryByUsername("select * from tb_user where username=?", user);
        if(result!=null)
        {

            if(result.getPassword().equals(password))
            {
                return resultMessage.returnMassage("login_success-"+user.getUsername());
            }else {
                return resultMessage.returnMassage("login_fail");
            }


        }else {

            return resultMessage.returnMassage("fail");
        }

    }
}
