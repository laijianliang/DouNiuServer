package com.fj.yd.Server.Accept;

import com.fj.yd.commons.JdbcTemplate_User;
import com.fj.yd.commons.ResultMessage;
import com.fj.yd.pojo.User;
import io.netty.buffer.ByteBuf;

public class AcceptRegister {




    public static ByteBuf handleRegister(String content)
    {
        ResultMessage resultMessage = new ResultMessage();
        User user = new User();
        //获取content中的username的值 :"register-username:admin&password:123456-_end"
        user.setUsername(content.split("&")[0].split(":")[1]);
        //获取content中的password的值
        user.setPassword(content.split("&")[1].split(":")[1]);
        int result = JdbcTemplate_User.InsertUser("insert into tb_user values(?,?)", user);
        if(result!=0)
        {
            return resultMessage.returnMassage("register_success");

        }else {

            return resultMessage.returnMassage("register_fail");
        }

    }

}
