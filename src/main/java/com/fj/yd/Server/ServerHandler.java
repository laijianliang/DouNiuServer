package com.fj.yd.Server;

import com.fj.yd.commons.ConvertByteBufToString;
import com.fj.yd.commons.ResultMessage;
import com.fj.yd.pojo.GameRoom;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.util.*;

public class ServerHandler  extends ChannelInboundHandlerAdapter  {

//    //ChannelGroup 是保存channel对象的
//    private  static ChannelGroup channelGroup = new
//            DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
private static ChannelGroup channels=new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
private  static Map<String,Object> map = new HashMap<String,Object>();
private static List<Channel> playerList = new ArrayList<Channel>();
    //游戏房间
  private Set<GameRoom> gameRooms =new HashSet<GameRoom>();



    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        Channel newchannel=ctx.channel();
        System.out.println("欢迎新客户端："+newchannel.remoteAddress());
        for(Channel ch:channels){
            if(ch!=newchannel){
                ch.writeAndFlush("欢迎新客户端："+newchannel.remoteAddress());
            }
        }
        channels.add(newchannel);
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        Channel newchannel =ctx.channel();
        for(Channel ch:channels) {
            if (ch != newchannel) {
                ch.writeAndFlush(newchannel.remoteAddress() + "退出聊天室");
            }
        }
        channels.remove(newchannel);
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if(entry.getValue().equals(newchannel))
            {
                map.remove(entry.getKey());
            }
        }

    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {


    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {

        System.out.println("dadsadas");
        System.out.println(channels.size());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {


        //当前Chanel
        Channel currentchannel = ctx.channel();
        //获取收到客户端发来的消息内容
        ByteBuf byteBuf = HandlerCenter.messageDistribution((String) msg);
        //处理登录成功后加入游戏总的线程Map
        //login_success-username_end
        //将返回给客户端的ByteBuf转换成字符串，以便处理返回逻辑
        String s = ConvertByteBufToString.convertByteBufToString(byteBuf);
//        ByteBuf messages = Unpooled.copiedBuffer(("login_success_end").getBytes());
//
//        channels.writeAndFlush(messages);

        if (s.startsWith("login_success"))
        {
           String username = s.split("-")[1].split("_")[0];
            map.put(username,ctx.channel());
            currentchannel.writeAndFlush(byteBuf);
        }
        //如果游戏开始就随机在在线的用户中选出三个玩家进行组队
//        if(s.startsWith("startgame"))
//        {
//
//            int size = channels.size();
//            if(size>=3)
//            {
//
//                int count =0;
//                for (Channel channel : channels) {
//                    if(count==2)
//                    {
//                        break;
//                    }
//                    if(channel!=ctx.channel())
//                    {
//                        playerList.add(channel);
//                        count++;
//                    }
//
//                }
//
//            }
//
//        }
       else if(s.startsWith("createroom"))
        {
            GameRoom gameRoom = new GameRoom();
            gameRoom.addChannel(ctx.channel());
            gameRooms.add(gameRoom);
            //获取房间的id
            int id = gameRoom.getId();
            //获取用户名称
            String username ="";
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                if(entry.getValue().equals(currentchannel))
                {
                    username = entry.getKey();
                }
            }
            //构造返回的数据格式
            ResultMessage resultMessage = new ResultMessage();
            ByteBuf byteBuf1 = resultMessage.returnMassage("createroom_success-" + id +"-"+ username);
            currentchannel.writeAndFlush(byteBuf1);
        }else if (s.startsWith("joinroom")){

           //获取客户端传来的roomid
            int id =Integer.parseInt(s.split("&")[1].split("-")[0]);
           //把当前Chanel加入对应的房间:joinroom-start&roomid-_end
           for(GameRoom gameRoom: gameRooms)
           {
               if(gameRoom.getId()==id)
               {
                   gameRoom.addChannel(currentchannel);
               }
           }
            //获取用户名称
            String username ="";
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                if(entry.getValue().equals(currentchannel))
                {
                    username = entry.getKey();
                }
            }
            //构造返回的数据格式
            ResultMessage resultMessage = new ResultMessage();
            ByteBuf byteBuf1 = resultMessage.returnMassage("joinroom_success-"+ username);
            currentchannel.writeAndFlush(byteBuf1);

        } else {
            currentchannel.writeAndFlush(byteBuf);
        }
//        for (int i=1;i<=playerList.size();i++)
//        {
//            Channel channel = playerList.get(i);
//            channel.writeAndFlush(messages);
//        }

//        for (Channel channel : channels) {
//            if(channel!=ctx.channel())
//            channel.writeAndFlush(messages);
//        }




//        System.out.println(msg);
//        Channel channel = ctx.channel();
//
//
//        //当前发送消息的客户端
//
//        //客户端发来的消息格式为：X-M(ss.ss)
//        //获取消息的第一个指令，根据指令接受响应的返回
//        String order = msg.toString().split("-")[0];
//        System.out.println(order);
//        if(order.equals("1")){
//            //若为X=1;则为登录验证
//            ByteBuf echo = Unpooled.copiedBuffer("登录验证！1_end".getBytes());
//            ctx.channel().writeAndFlush(echo);
//        }
//        if(order.equals("2"))
//        {
//            ByteBuf echo = Unpooled.copiedBuffer("登录验证！2_end".getBytes());
//            ctx.channel().writeAndFlush(echo);
//
//        }
//        if(order.equals("3"))
//        {
//            ByteBuf echo = Unpooled.copiedBuffer("登录验证！3_end".getBytes());
//            ctx.channel().writeAndFlush(echo);
//        }
//        if(order.equals("4"))
//        {
//            ByteBuf echo = Unpooled.copiedBuffer("登录验证！4_end".getBytes());
//            ctx.channel().writeAndFlush(echo);
//        }
//        System.out.println("dsa2");

    }


}
