package com.fj.yd.Server;

import com.fj.yd.Server.Accept.*;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class HandlerCenter {


    public static ByteBuf messageDistribution(String mes)
    {

        //客户端发来的消息格式为：X-M(ss.ss)
        //获取消息的第一个指令，根据指令接受响应的返回
        //order :规定的指令(消息头)  login-username:admin&password:123456-_end
        String order = mes.split("-")[0];
        //content ：消息体
        String content = mes.split("-")[1].split("-")[0];
        ByteBuf message = Unpooled.copiedBuffer(("").getBytes());

        switch (order) {
            case "login":
                message = AcceptLogin.handleLogin(content);
                break;
            case "register":
                message = AcceptRegister.handleRegister(content);
                break;
            case "startgame":
                message = AcceptStartGame.startGame(content);
                break;
            case "createroom":
                message = AcceptCreateRoom.handleCreateRoom(content);
                break;
            case "joinroom":
                message = AcceptJoinRoom.handleJoinRoom(mes);
                break;
            default:
                break;
                    }

        return message;
    }
}
